package io.dmendezg.album.version;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by MED1IMB on 1/21/2017.
 */
@RestController
@RequestMapping("public")
public class VersionController {

	public static final String VERSION = "1.0";

	@RequestMapping("/version")
	@ResponseBody
	public ResponseEntity<?> getVersion() {
		return new ResponseEntity<>(VERSION, HttpStatus.OK);
	}

}
