package io.dmendezg.album;
import io.dmendezg.album.media.MediaRepository;
import io.dmendezg.album.tag.TagRepository;
import io.dmendezg.album.tag.TagSearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;

/**
 * Created by MED1IMB on 12/16/2016.
 */
public class Repositories {

	@Autowired
	protected GridFsTemplate template;

	@Autowired
	protected MediaRepository mediaRepository;

	@Autowired
	protected TagRepository tagRepository;

	@Autowired
	protected TagSearchRepository tagSearchRepository;

}
