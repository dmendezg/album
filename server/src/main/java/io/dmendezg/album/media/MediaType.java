package io.dmendezg.album.media;
/**
 * Created by MED1IMB on 12/9/2016.
 */
public enum MediaType {

	PNG, JPEG, JPG, MP4

}
