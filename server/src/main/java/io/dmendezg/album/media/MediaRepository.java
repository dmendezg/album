package io.dmendezg.album.media;
import io.dmendezg.album.tag.Tag;
import io.dmendezg.album.user.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
/**
 * Created by MED1IMB on 12/9/2016.
 */
public interface MediaRepository extends MongoRepository<Media, String> {

	List<Media> findByTagsInAndUser(List<Tag> tags, User user);

	Media findByIdAndUser(String id, User user);

}
