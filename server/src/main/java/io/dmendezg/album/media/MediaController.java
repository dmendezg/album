package io.dmendezg.album.media;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import io.dmendezg.album.Repositories;
import io.dmendezg.album.tag.Tag;
import io.dmendezg.album.tag.TagSearch;
import io.dmendezg.album.user.User;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static io.dmendezg.album.model.RequestParams.*;
import static io.dmendezg.album.model.WebParams.API_V1;
import static io.dmendezg.album.model.WebParams.MEDIA;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.IMAGE_JPEG;

/**
 * Created by MED1IMB on 12/9/2016.
 */
@RestController
@RequestMapping(API_V1)
public class MediaController extends Repositories {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private static ObjectMapper mapper = new ObjectMapper();

	@PostMapping(MEDIA)
	@ResponseBody
	public ResponseEntity<?> addMedia(@AuthenticationPrincipal User user,
			@RequestParam(FILE) MultipartFile file, @RequestPart(TAGS) String tagNames) {
		try {
			GridFSFile gridFSFile = template
					.store(file.getInputStream(), file.getOriginalFilename());
			Set<Tag> iTags = mapper.readValue(tagNames, new TypeReference<Set<Tag>>() {
			});
			Set<Tag> tags = iTags.stream().map(tag -> {
				Tag eTag = tagRepository.findByName(tag.getName());
				return eTag == null ? tag : eTag;
			}).collect(Collectors.toSet());
			tags.forEach(Tag::addVote);
			Media media = new Media(file.getOriginalFilename(), MediaType.JPG,
					gridFSFile.getId().toString(), tags, user);
			media = mediaRepository.save(media);
			return new ResponseEntity<>(media, CREATED);
		} catch (IOException e) {
			logger.error("", e);
		}
		return new ResponseEntity<>(null, INTERNAL_SERVER_ERROR);
	}

	@GetMapping(MEDIA)
	@ResponseBody
	public ResponseEntity<?> getMedia(@AuthenticationPrincipal User user,
			@RequestParam(name = TAGS, required = false) List<String> tags) {
		List<Tag> tagObjs = new ArrayList<>(tagRepository.findByNameIn(tags));
		if (tags != null) {
			List<Media> mediaObjs = mediaRepository.findByTagsInAndUser(tagObjs, user);
			if (mediaObjs.size() > 0) {
				tagSearchRepository.save(new TagSearch(tagObjs, new Date()));
				return new ResponseEntity<>(
						mediaObjs.stream().filter(m -> m.getTags().containsAll(tagObjs))
								.collect(Collectors.toList()), OK);
			}
		}
		return new ResponseEntity<>(Arrays.asList(), OK);
	}

	@GetMapping(MEDIA + "/" + ID_PH)
	@ResponseBody
	public ResponseEntity<?> getMediaStream(@PathVariable(ID) String id,
			@RequestParam(required = false) Map<String, String> options) {
		try {
			GridFSDBFile file = template.findOne(Query.query(Criteria.where(MONGO_ID).is(id)));
			final HttpHeaders headers = new HttpHeaders();
			headers.setContentType(IMAGE_JPEG);
			return new ResponseEntity<>(IOUtils.toByteArray(file.getInputStream()), headers, OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(null, INTERNAL_SERVER_ERROR);
	}

	@PutMapping(MEDIA + "/" + ID_PH)
	@ResponseBody
	public ResponseEntity<?> updateMedia(@AuthenticationPrincipal User user,
			@PathVariable(ID) String id, @RequestBody Media media) {
		try {
			Media eMedia = mediaRepository.findByIdAndUser(id, user);
			eMedia.setTags(media.getTags());
			return ResponseEntity.ok(mediaRepository.save(eMedia));
		} catch (Exception e) {
			logger.error("", e);
		} return new ResponseEntity<>(null, INTERNAL_SERVER_ERROR);
	}

}
