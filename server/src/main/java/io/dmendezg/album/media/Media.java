package io.dmendezg.album.media;

import io.dmendezg.album.model.CascadeSave;
import io.dmendezg.album.model.Resource;
import io.dmendezg.album.tag.Tag;
import io.dmendezg.album.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by MED1IMB on 12/9/2016.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class Media extends Resource {

	private String displayName;

	private MediaType mediaType;

	@Indexed(unique = true)
	private String mediaId;

	@DBRef
	@CascadeSave
	private Set<Tag> tags = new HashSet<>();

	@DBRef
	private User user;
	
}
