package io.dmendezg.album.auth;
import io.dmendezg.album.user.User;
import lombok.Data;
import org.springframework.social.connect.Connection;
import org.springframework.social.oauth2.AccessGrant;
/**
 * Created by MED1IMB on 4/13/2017.
 */

@Data
public abstract class SocialService {

	protected Connection connection;

	abstract boolean createConnection(AccessGrant accessGrant);

	abstract User getUser();

}
