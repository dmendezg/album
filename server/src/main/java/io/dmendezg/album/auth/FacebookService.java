package io.dmendezg.album.auth;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.stereotype.Component;
/**
 * Created by MED1IMB on 4/13/2017.
 */

@Component
public class FacebookService extends SocialService {

	@Autowired
	private FacebookConnectionFactory facebookConnectionFactory;

	@Override
	public boolean createConnection(AccessGrant accessGrant) {
		try {
			this.connection = facebookConnectionFactory.createConnection(accessGrant);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public io.dmendezg.album.user.User getUser() {
		if (connection == null)
			return null;
		Facebook facebook = (Facebook) connection.getApi();
		String[] fields = {"id", "email", "first_name", "last_name"};
		User fUser = facebook.fetchObject("me", User.class, fields);
		io.dmendezg.album.user.User user = io.dmendezg.album.user.User.builder()
				.userId(fUser.getId()).username(fUser.getFirstName() + " " + fUser.getLastName())
				.build();
		return user;
	}

}
