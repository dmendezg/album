package io.dmendezg.album.auth;
/**
 * Created by MED1IMB on 1/21/2017.
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.security.AuthenticationNameUserIdSource;

/**
 * Created by chi0696 on 10/4/2016.
 */
@Configuration
@EnableSocial
public class SocialConfig implements SocialConfigurer {

	@Bean
	public FacebookConnectionFactory facebookConnectionFactory() {
		FacebookConnectionFactory facebookConnectionFactory = new FacebookConnectionFactory(
				"245187842559482", "64c54fe05510c5005a3fe6c10d376170");
		facebookConnectionFactory.setScope("email");
		return facebookConnectionFactory;
	}

	@Override
	public void addConnectionFactories(ConnectionFactoryConfigurer cfConfig, Environment env) {
		cfConfig.addConnectionFactory(facebookConnectionFactory());
	}

	@Override
	public UserIdSource getUserIdSource() {
		return new AuthenticationNameUserIdSource();
	}

	@Override
	public UsersConnectionRepository getUsersConnectionRepository(
			ConnectionFactoryLocator connectionFactoryLocator) {
		return new InMemoryUsersConnectionRepository(connectionFactoryLocator);
	}

}
