package io.dmendezg.album.auth;

import io.dmendezg.album.user.User;
import io.dmendezg.album.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.RevokedAuthorizationException;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by chi0696 on 10/4/2016.
 */
@Component
public class AuthenticationTokenFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationTokenFilter.class);

    @Autowired
    private SocialService socialService;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        SecurityContext context = SecurityContextHolder.getContext();
        if (context.getAuthentication() == null || !context.getAuthentication().isAuthenticated()) {
            String token = request.getHeader("auth-token");
            if (token == null)
                token = request.getParameter("auth-token");
            if (token != null && !StringUtils.isEmpty(token) && !"undefined".equals(token)) {
                AccessGrant accessGrant = new AccessGrant(token);
                try {
                    if (!socialService.createConnection(accessGrant))
                        throw new BadCredentialsException(
                                "User could not authenticate against Social Network");
                    User sUser = socialService.getUser();
                    if (sUser == null)
                        throw new BadCredentialsException(
                                "There was an error fetching user information.");
                    User user = userRepository.findByUserId(sUser.getUserId());
                    if (user == null)
                        user = userRepository.save(sUser);
                    SocialAuthenticationToken socialToken = new SocialAuthenticationToken(
                            socialService.getConnection(), user, new HashMap<>(), Arrays.asList());
                    SecurityContextHolder.getContext().setAuthentication(socialToken);
                } catch (RevokedAuthorizationException e) {
                    throw new BadCredentialsException(e.getMessage());
                } catch (Exception e) {
                    throw new BadCredentialsException(e.getMessage());
                }
            } else {
                User user = userRepository.findByUserId(null);
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                        user, "pass");
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }
        }
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy() {
    }

}