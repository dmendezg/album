package io.dmendezg.album.mongo;
/**
 * Created by MED1IMB on 12/16/2016.
 */
import io.dmendezg.album.model.CascadeSave;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Collection;

@Data
@AllArgsConstructor
public class CascadeCallback implements ReflectionUtils.FieldCallback {

	private Object source;
	private MongoOperations mongoOperations;

	@Override
	public void doWith(final Field field) throws IllegalArgumentException, IllegalAccessException {
		ReflectionUtils.makeAccessible(field);
		if (field.isAnnotationPresent(DBRef.class) && field
				.isAnnotationPresent(CascadeSave.class)) {
			final Object fieldValue = field.get(getSource());
			if (fieldValue != null) {
				final FieldCallback callback = new FieldCallback();
				ReflectionUtils.doWithFields(fieldValue.getClass(), callback);
				if (fieldValue instanceof Collection)
					((Collection) fieldValue).forEach(value -> getMongoOperations().save(value));
				else
					getMongoOperations().save(fieldValue);
			}
		}
	}

}