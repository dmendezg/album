package io.dmendezg.album.mongo;
import org.springframework.data.annotation.Id;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

/**
 * Created by MED1IMB on 12/16/2016.
 */
public class FieldCallback implements ReflectionUtils.FieldCallback {

	private boolean idFound;

	public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
		ReflectionUtils.makeAccessible(field);
		if (field.isAnnotationPresent(Id.class)) {
			idFound = true;
		}
	}

	public boolean isIdFound() {
		return idFound;
	}
}