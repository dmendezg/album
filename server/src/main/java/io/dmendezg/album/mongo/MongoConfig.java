package io.dmendezg.album.mongo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * Created by MED1IMB on 12/16/2016.
 */
@Configuration
public class MongoConfig {

	@Bean
	public CascadeSaveMongoEventListener userCascadingMongoEventListener() {
		return new CascadeSaveMongoEventListener();
	}

}
