package io.dmendezg.album.model;
/**
 * Created by MED1IMB on 12/16/2016.
 */
public class RequestParams {

	public static final String FILE = "file";
	public static final String TAGS = "tags";
	public static final String ID = "id";
	public static final String ID_PH = "{id}";
	public static final String MONGO_ID = "_id";

}
