package io.dmendezg.album.model;
/**
 * Created by MED1IMB on 12/16/2016.
 */
public class WebParams {

	public static final String API_V1 = "api/v1";

	public static final String MEDIA = "media";
	public static final String TAGS = "tags";

	public static final String QUERY = "q";
	public static final String FREQUENT_QUERY = "fq";

}
