package io.dmendezg.album.model;

import org.springframework.data.annotation.Id;
import lombok.Data;

/**
 * Created by MED1IMB on 12/9/2016.
 */
@Data
public abstract class Resource {

	@Id
	private String id;

}
