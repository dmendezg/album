package io.dmendezg.album.tag;
import io.dmendezg.album.Repositories;
import io.dmendezg.album.user.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static io.dmendezg.album.model.WebParams.*;
import static org.springframework.http.HttpStatus.OK;

/**
 * Created by MED1IMB on 12/16/2016.
 */
@RestController
@RequestMapping(API_V1)
public class TagController extends Repositories {

	@GetMapping(TAGS)
	@ResponseBody
	public ResponseEntity<?> searchTags(@AuthenticationPrincipal User user,
			@RequestParam Map<String, String> options) {
		if (options.containsKey(QUERY))
			return new ResponseEntity<>(tagRepository.findByNameStartsWith(options.get(QUERY)), OK);
		if (options.containsKey(FREQUENT_QUERY))
			return new ResponseEntity<>(tagSearchRepository.findByPopularity(), OK);
		return new ResponseEntity<>(tagRepository.findAll(), OK);
	}

}
