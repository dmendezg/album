package io.dmendezg.album.tag;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
/**
 * Created by MED1IMB on 3/6/2017.
 */
public class TagSearchRepositoryImpl implements TagSearchRepositoryCustom {

	private ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private MongoOperations ops;

	@Override
	public List<Tag> findByPopularity() {
		DBObject project = new BasicDBObject("_id", 0);
		project.put("tags", 1);
		DBObject $project = new BasicDBObject("$project", project);
		DBObject $unwind = new BasicDBObject("$unwind", "$tags");
		DBObject group = new BasicDBObject("_id", "$tags");
		group.put("count", new BasicDBObject("$sum", 1));
		DBObject $group = new BasicDBObject("$group", group);
		DBObject $sort = new BasicDBObject("$sort", new BasicDBObject("count", -1));
		List<Tag> tags = new ArrayList<>();
		ops.getCollection("tagSearches").aggregate(Arrays.asList($project, $unwind, $group, $sort))
				.results().forEach(r -> {
			Tag tag = ops.findById(((DBRef) r.get("_id")).getId(), Tag.class);
			tag.setPopularity((Integer) r.get("count"));
			tags.add(tag);
		});
		return tags;
	}

}
