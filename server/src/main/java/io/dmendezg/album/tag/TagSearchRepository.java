package io.dmendezg.album.tag;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TagSearchRepository
		extends MongoRepository<TagSearch, String>, TagSearchRepositoryCustom {

	Iterable<TagSearch> findByTagsIn(Tag tag);

}
