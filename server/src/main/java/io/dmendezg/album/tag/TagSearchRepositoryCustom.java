package io.dmendezg.album.tag;
import java.util.List;
/**
 * Created by MED1IMB on 3/6/2017.
 */
public interface TagSearchRepositoryCustom {

	List<Tag> findByPopularity();

}
