package io.dmendezg.album.tag;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Collection;
import java.util.Set;
/**
 * Created by MED1IMB on 12/16/2016.
 */
public interface TagRepository extends MongoRepository<Tag, String> {

	Tag findByName(String name);

	Set<Tag> findByNameIn(Collection<String> names);

	Set<Tag> findByNameStartsWith(String token);

}
