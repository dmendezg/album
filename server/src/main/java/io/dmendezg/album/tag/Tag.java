package io.dmendezg.album.tag;

import io.dmendezg.album.model.Resource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by MED1IMB on 12/9/2016.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tags")
public class Tag extends Resource {

	@Indexed(unique = true)
	private String name;

	private long popularity = 0;

	public Tag(String name) {
		this.name = name;
	}

	public void addVote() {
		this.popularity++;
	}

}
