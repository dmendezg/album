package io.dmendezg.album;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.junit.AfterClass;
import org.junit.BeforeClass;
/**
 * Created by MED1IMB on 12/16/2016.
 */
public abstract class AbstractMongoDBTest extends Repositories {

	/**
	 * please store Starter or RuntimeConfig in a static final field
	 * if you want to use artifact store caching (or else disable caching)
	 */
	private static final MongodStarter starter = MongodStarter.getDefaultInstance();

	private static MongodExecutable _mongodExe;
	private static MongodProcess _mongod;

	@BeforeClass
	public static void setUp() throws Exception {
		_mongodExe = starter.prepare(new MongodConfigBuilder().version(Version.Main.PRODUCTION)
				.net(new Net("localhost", 12345, Network.localhostIsIPv6())).build());
		_mongod = _mongodExe.start();
	}

	@AfterClass
	public static void tearDown() throws Exception {
		_mongod.stop();
		_mongodExe.stop();
	}

}
