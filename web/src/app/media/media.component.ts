import {Component} from "@angular/core";
import {Observable} from "rxjs";
import {URLSearchParams, Headers, Response} from "@angular/http";
import {FileUploader} from "ng2-file-upload";
import {environment} from "../../environments/environment";
import {HttpClient} from "../http/http.service";

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.css']
})
export class MediaComponent {

  headers: Headers;
  autocompleteItems = [];

  public uploader: FileUploader = new FileUploader({});
  public hasBaseDropZoneOver: boolean = false;

  constructor(public http: HttpClient) {
    var that = this;
    this.headers = new Headers();
    this.headers.set('Content-Type', 'multipart/form-data');
    this.uploader.onAfterAddingFile = function (fileItem) {
      that.loadPreview(fileItem);
    };
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  loadPreview(file): void {
    if (!file) return;
    var reader = new FileReader();
    reader.addEventListener("load", function () {
      file.url = reader.result;
    }, false);
    if (file._file) {
      reader.readAsDataURL(file._file);
    }
  }

  saveMedia(item: any) {
    var that = this;
    this.http.createAuthorizationHeader(this.headers);
    return Observable.create(observer => {
      let formData: FormData = new FormData(),
        xhr: XMLHttpRequest = new XMLHttpRequest();
      formData.append("tags", JSON.stringify(item.tags.map(function (tag) {
        return {
          name: tag.name
        }
      })));
      formData.append("file", item._file);
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 201) {
            observer.next(JSON.parse(xhr.response));
            observer.complete();
          } else {
            observer.error(xhr.response);
          }
        }
      };
      xhr.open('POST', environment.backendUrl + '/api/v1/media', true);
      xhr.setRequestHeader('auth-token', this.http.token);
      xhr.send(formData);
    }).subscribe(() => {
      var index = that.uploader.queue.indexOf(item);
      if (index > -1)
        that.uploader.queue.splice(index, 1);
    })
  }
  //
  // onTextChange(event, item) {
  //   this.getTags(event, item);
  // }

  public getAutocompleteTags = (text: string): Observable<Response> => {
    const url = environment.backendUrl + `/api/v1/tags?q=${text}`;
    return this.http.get(url).map(data => data.json());
  };

}
