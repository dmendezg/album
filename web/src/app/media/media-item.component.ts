import {Component, OnInit, Input} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Http, Headers, URLSearchParams} from "@angular/http";
import {FileUploader} from "ng2-file-upload";
import {Observable} from "rxjs";
import {Media} from "../media/media";
import {HttpClient} from "../http/http.service";
import {environment} from "../../environments/environment";

@Component({
  selector: 'media-item',
  templateUrl: './media-item.component.html',
  styleUrls: ['./media-item.component.css']
})

export class MediaItemComponent implements OnInit {

  @Input() media;

  backendUrl = environment.backendUrl;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    // var that = this;
    // var params = new URLSearchParams();
    // params.set('base64', true);
    // this.http.get(environment.backendUrl + '/api/v1/media/' + this.media.mediaId, {search: params}).subscribe((response) => {
    //     that.media.content = 'data:image/jpg;base64,' + response._body;
    //   }
    // );
  }

}
