export interface Tag {
  name: string;
}

export interface Media {
  displayName: string;
  mediaType: string;
  mediaId: string;
  tags: Array<Tag>;
}
