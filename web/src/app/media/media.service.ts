/**
 * Created by MED1IMB on 5/24/2017.
 */
import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "../http/http.service";

@Injectable()
export class MediaService {

  constructor(private http: HttpClient) {
  }

  updateMedia(media) {
    return this.http.put(environment.backendUrl + '/api/v1/media/' + media.id, media);
  }

}
