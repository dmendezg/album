import {Component} from "@angular/core";
import {User, Auth} from "../auth/auth.service";
import {Subscription} from "rxjs";
/**
 * Created by chi0696 on 10/7/2016.
 */
declare var $: any;

@Component({
  selector: 'user-dropdown',
  templateUrl: 'user-dropdown.component.html'
})

export class UserDropdown {

  user = new User();
  _userSubscription: Subscription;

  constructor(public auth: Auth) {
    this.user = auth.user;
    this._userSubscription = auth.userChange.subscribe((newUser) => {
      this.user = newUser;
    });
  }

  ngOnDestroy() {
    this._userSubscription.unsubscribe();
  }

  ngOnInit() {
    $('.ui.dropdown').dropdown();
  }

}
