import {BrowserModule} from "@angular/platform-browser";
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule, Http} from "@angular/http";
import {AppComponent} from "./app.component";
import {TagInputModule} from "ngx-chips";
import {FileUploadModule} from "ng2-file-upload";
import {MasonryModule} from "angular2-masonry";
import {Routes, RouterModule} from "@angular/router";
import {LocationStrategy, HashLocationStrategy} from "@angular/common";
import {HomeComponent} from "./home/home.component";
import {MediaComponent} from "./media/media.component";
import {AlbumComponent} from "./album/album.component";
import {AlbumService} from "./album/album.service";
import {Auth} from "./auth/auth.service";
import {UserDropdown} from "./menu/user-dropdown.component";
import {HttpClient} from "./http/http.service";
import {MediaItemComponent} from "./media/media-item.component";
import {TagsFilter} from "./album/tags.filter";
import {AuthGuard} from "./_guards/auth.guard";
import {TranslateModule, TranslateLoader} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MediaService} from "./media/media.service";
import {FacebookModule} from "ngx-facebook";

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'media', component: MediaComponent, canActivate: [AuthGuard]},
  {path: 'album', component: AlbumComponent, canActivate: [AuthGuard]},
];

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, "assets/i18n/", ".json");
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AlbumComponent,
    MediaComponent,
    MediaItemComponent,
    UserDropdown,
    TagsFilter
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    TagInputModule,
    MasonryModule,
    FileUploadModule,
    FacebookModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    RouterModule.forRoot(routes)],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }, AlbumService, MediaService, Auth, HttpClient, AuthGuard],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})

export class AppModule {
}
