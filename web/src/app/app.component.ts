import {Component, OnInit} from "@angular/core";
import {Auth} from "./auth/auth.service";
import {TranslateService} from "@ngx-translate/core";
import {Subscription} from "rxjs";

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  user;
  _userSubscription: Subscription;
  loginStatus: boolean = false;

  links = [{
    name: 'home',
    displayName: 'Home'
  }, {
    name: 'album',
    displayName: 'Album'
  }, {
    name: 'media',
    displayName: 'Media'
  }]

  constructor(public auth: Auth, translate: TranslateService) {
    var that = this;
    translate.setDefaultLang('en');
    translate.use('en');
    this._userSubscription = auth.userChange.subscribe((newUser) => {
      that.user = newUser;
    });
  }

  ngOnInit(): void {
    $('.masthead').visibility({
      once: false,
      onBottomPassed: function () {
        $('.fixed.menu').transition('fade in');
      },
      onBottomPassedReverse: function () {
        $('.fixed.menu').transition('fade out');
      }
    });

    // create sidebar and attach to menu open
    $('.ui.sidebar')
      .sidebar('attach events', '.toc.item');
  }

  openLogin() {
    $('.ui.modal').modal('show');
  }

  closeLogin() {
    $('.ui.modal').modal('hide');
  }

}
