import {Injectable} from "@angular/core";
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Auth} from "../auth/auth.service";
import {Observable, Subscription} from "rxjs";

@Injectable()
export class AuthGuard implements CanActivate {

  _userSubscription: Subscription;

  constructor(private router: Router, private auth: Auth) {
    this._userSubscription = auth.userChange.subscribe((newUser) => {
      if (newUser == null)
        this.router.navigate(['/home']);
    });
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.auth.user !== null || this.auth.getLoginStatus().then(response => {
      if (response && response.status === 'connected') {
        return true;
      } else {
      }
    }).catch(() => {
      this.router.navigate(['/home'], {queryParams: {returnUrl: state.url}});
      return false;
    });
  }

}
