import {Component, OnInit} from "@angular/core";

declare var jQuery: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  mediaList = [];

  constructor() {
    var temp = [];
    for (var i = 0; i < 40; i++) {
      temp[i] = {
        id: i,
        url: this.getImageUrl(i)
      };
    }
    this.mediaList = temp;
  }

  getImageUrl(media) {
    var width = 300;
    var heigth = 100 * (Math.floor(Math.random() * (4 - 2 + 1)) + 2);
    return 'http://lorempixel.com/' + width + '/' + heigth + '/city/?' + (media / 12);
  }

  ngOnInit() {
    function widthChanged(mq) {
      if (mq.matches) slider.glide(jQuery.extend(options, {paddings: '18%'}));
      else slider.glide(options);
    }

    function coverflow(data) {
      data.current.removeClass('pre following')
        .nextAll()
        .removeClass('pre following')
        .addClass('following')
        .end()
        .prevAll()
        .removeClass('pre following')
        .addClass('pre');
    }

    var slider = jQuery('#Glide');
    var options = {
      type: 'carousel',
      autoplay: false,
      paddings: '25%',
      afterInit: function (data) {
        coverflow(data);
      },
      afterTransition: function (data) {
        coverflow(data);
      }
    };

    if (matchMedia) {
      var mq = window.matchMedia("(max-width: 768px)");
      mq.addListener(widthChanged);
      widthChanged(mq);
    } else {
      slider.glide(options);
    }
  }

}
