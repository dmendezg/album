import {Component, OnInit} from "@angular/core";
import {URLSearchParams, Response} from "@angular/http";
import {environment} from "../../environments/environment";
import {HttpClient} from "../http/http.service";
import {Auth} from "../auth/auth.service";
import {Subscription, Observable} from "rxjs";
import {MediaService} from "../media/media.service";

declare var jQuery: any;
declare var imagesLoaded: any;

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {

  backendUrl = environment.backendUrl;
  activated = false;
  editMediaId = "-1";
  mediaBackup;

  mediaList = [];
  accessQueryParam = "";
  popularItems = [];
  tags = [];

  _userSubscription: Subscription;

  constructor(public auth: Auth, public http: HttpClient, public mediaService: MediaService) {
    this._userSubscription = auth.userChange.subscribe((user) => {
      if (user != null)
        this.ngOnInit();
    });
  }

  public getAutocompleteTags = (text: string): Observable<Response> => {
    const url = environment.backendUrl + `/api/v1/tags?q=${text}`;
    return this.http.get(url).map(data => data.json());
  };

  getPopularTags() {
    var params = new URLSearchParams();
    params.set('fq', 'true');
    this.http.get(environment.backendUrl + '/api/v1/tags', {search: params}).subscribe((response) => {
      this.popularItems = response.json();
    })
  }

  onAdd() {
    var that = this;
    this.activated = false;
    var params = new URLSearchParams();
    this.tags.forEach((tag) => params.append('tags', tag.name || tag));
    this.mediaList = [];
    this.http.get(environment.backendUrl + '/api/v1/media', {search: params}).subscribe((response) => {
        response.json().forEach(function (item) {
          that.mediaList.push(item);
        });
        this.activated = true;
      }
    );
  }

  editMedia(media) {
    this.editMediaId = media.mediaId;
    this.mediaBackup = Object.assign({}, media);
  }

  saveMedia(media) {
    this.mediaService.updateMedia(media).subscribe((response) => {
        console.log(response);
        this.editMediaId = '-1';
      }
    );
  }

  cancelEditMedia(media) {
    Object.assign(media, this.mediaBackup);
    this.editMediaId = '-1';
  }

  onRemove() {
    this.onAdd();
  }

  selectTag(tag) {
    var index = this.tags.indexOf(tag);
    if (index > -1)
      this.tags.splice(index, 1);
    else
      this.tags.push(tag);
    this.onAdd();
  }

  initSlider() {
    function widthChanged(mq) {
      if (mq.matches) slider.glide(jQuery.extend(options, {paddings: '18%'}));
      else slider.glide(options);
    }

    function coverflow(data) {
      data.current.removeClass('pre following')
        .nextAll()
        .removeClass('pre following')
        .addClass('following')
        .end()
        .prevAll()
        .removeClass('pre following')
        .addClass('pre');
    }

    var slider = jQuery('#Glide');
    var options = {
      type: 'slider',
      autoplay: false,
      paddings: '20%',
      afterInit: function (data) {
        coverflow(data);
      },
      afterTransition: function (data) {
        coverflow(data);
      }
    };

    // media query event handler
    if (matchMedia) {
      var mq = window.matchMedia("(max-width: 768px)");
      mq.addListener(widthChanged);
      widthChanged(mq);
    } else {
      slider.glide(options);
    }

    setTimeout(function () {
      slider.removeClass('is-loading')
        .find('.glide__wrapper')
        .removeClass('is-hidden')
        .addClass('is-visible');
    }, 100);
  }


  open() {
    jQuery('.page').dimmer('show');
    setTimeout(this.initSlider, 1);
  }

  display(media) {
    setTimeout(function () {
      media.show = true;
    }, 100)
  }

  ngOnInit(): void {
    this.initSlider();
    if (this.http.token)
      this.accessQueryParam = 'auth-token=' + this.http.token;
    this.getPopularTags();
  }

}
