import {PipeTransform, Injectable, Pipe} from "@angular/core";

@Pipe({
  name: 'tags',
  pure: false
})
@Injectable()
export class TagsFilter implements PipeTransform {
  transform(items: any[], args: any[]): any {
    return items.filter(item => args.indexOf(item.name) < 0);
  }
}
