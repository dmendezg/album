/**
 * Created by MED1IMB on 1/13/2017.
 */
import {Injectable} from '@angular/core';

@Injectable()
export class AlbumService {

  tags = [];

  saveTags(tags) {
    this.tags = tags;
  }

  getTags() {
    return this.tags;
  }

}
