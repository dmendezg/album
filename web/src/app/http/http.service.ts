import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptionsArgs} from '@angular/http';

@Injectable()
export class HttpClient {

  token: string;

  constructor(private http: Http) {
  }

  createAuthorizationHeader(headers: Headers) {
    headers.append('auth-token', this.token);
  }

  setToken(token) {
    this.token = token;
  }

  get(url, options?: RequestOptionsArgs) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    if (!options) options = {};
    options.headers = headers
    return this.http.get(url, options);
  }

  post(url, data) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.post(url, data, {
      headers: headers
    });
  }

  put(url, data?) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(url, data, {
      headers: headers
    });
  }

}
