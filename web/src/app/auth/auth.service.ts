import {Injectable} from "@angular/core";
import {FacebookService, InitParams} from "ngx-facebook";
import {BehaviorSubject} from "rxjs";
import {HttpClient} from "../http/http.service";
import {Router} from "@angular/router";
import {environment} from "../../environments/environment";

export class User {
  id: string;
  name: string;
  username: string;
  email: string;
  authorities: any;
  key: any;
  logged: boolean;
  token: string;

  constructor(obj?: any) {
    this.id = obj && obj.id || null;
    this.name = obj && obj.name || null;
    this.username = obj && obj.username || null;
    this.email = obj && obj.email || null;
    this.authorities = obj && obj.authorities || [];
    this.key = obj && obj.key || {};
    this.logged = false;
    this.token = obj && obj.id || null;
  }

  getProfilePictureUrl() {
    if (this.key.providerId == null)
      return "assets/img/user.png";
    switch (this.key.providerId) {
      case "facebook":
        return 'http://graph.facebook.com/' + this.key.providerUserId + '/picture?type=square';
      default:
        return "";
    }
  }
}

@Injectable()
export class Auth {

  user = new User();
  userChange = new BehaviorSubject<User>(null);

  constructor(public fb: FacebookService, private http: HttpClient, private router: Router) {
    let fbParams: InitParams = {
      appId: environment.facebookClientId,
      xfbml: true,
      version: 'v2.8'
    };
    this.fb.init(fbParams);
    this.getLoginStatus();
  }

  getLoginStatus(): any {
    switch (localStorage.getItem('provider')) {
      case 'facebook':
        return this.getFacebookLoginStatus();
      case 'local':
        return this.getLocalLoginStatus();
      default :
        return Promise.resolve(false);
    }
  }

  logout() {
    switch (localStorage.getItem('provider')) {
      case 'facebook':
        this.logoutFacebook();
        break;
      case 'local':
        this.logoutLocal();
    }
  }

  clearUser() {
    this.user = null;
    this.userChange.next(null);
    this.http.setToken(null);
    localStorage.setItem('provider', null);
  }

  authenticated() {
    return this.user !== null;
  }

  /**
   * Facebook User
   */

  loginFacebook(): void {
    this.fb.login().then((response) => {
        this.processFacebookResponse(response);
        this.router.navigate(['/album']);
      },
      (error: any) => console.error(error)
    );
  }

  getFacebookLoginStatus() {
    return this.fb.getLoginStatus().then((response) => {
        this.processFacebookResponse(response);
      }
    );
  }

  processFacebookResponse(response) {
    if (response.status === 'connected') {
      this.http.setToken(response.authResponse.accessToken);
      this.getFacebookUserDetails(response).then((user) => {
        this.setFacebookAuthentication(user)
      });
    }
  }

  getFacebookUserDetails(loginResponse) {
    return this.fb.api("/" + loginResponse.authResponse.userID);
  }

  setFacebookAuthentication(facebookUser) {
    var user = new User({
      id: facebookUser.id,
      name: facebookUser.name,
      key: {
        providerId: 'facebook',
        providerUserId: facebookUser.id
      }
    });
    localStorage.setItem('provider', 'facebook');
    this.user = user;
    this.userChange.next(user);
  }

  logoutFacebook() {
    this.fb.logout().then(
      () => {
        this.clearUser()
      },
      (error: any) => console.error(error)
    );
  }

  /**
   * Local User
   */

  loginWithTestUser() {
    var that = this;
    this.setLocalAuthentication();
    setTimeout(function() {
      that.router.navigate(['/album']);
    }, 100);
  }

  getLocalLoginStatus() {
    this.setLocalAuthentication();
    return Promise.resolve({
      status: 'connected'
    });
  }

  setLocalAuthentication() {
    var user = new User({
      name: 'John Doe',
      key: {
        providerId: undefined
      }
    });
    localStorage.setItem('provider', 'local');
    this.user = user;
    this.userChange.next(this.user);
  }

  logoutLocal() {
    this.clearUser()
  }

}
